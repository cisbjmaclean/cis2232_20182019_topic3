package info.hccis.camper.bo;

/**
 * This will contain business logic associated with our hello world application.
 * @author bjmaclean
 * @since Oct 6, 2017
 */
public class HelloBO {
    public void sayHelloToConsole(String name){
        System.out.println("Hello "+name);
    }
}
