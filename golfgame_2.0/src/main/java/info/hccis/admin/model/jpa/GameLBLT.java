/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author BJMacLean
 */
@Entity
@Table(name = "GameLBLT")
@NamedQueries({
    @NamedQuery(name = "GameLBLT.findAll", query = "SELECT g FROM GameLBLT g")})
public class GameLBLT implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "gameId")
    private Integer gameId;
    @Size(max = 100)
    @Column(name = "team1presses")
    private String team1presses;
    @Size(max = 100)
    @Column(name = "team2presses")
    private String team2presses;
    @Size(max = 100)
    @Column(name = "team1GreenyPoints")
    private String team1GreenyPoints;
    @Size(max = 100)
    @Column(name = "team2GreenyPoints")
    private String team2GreenyPoints;
    @Size(max = 100)
    @Column(name = "team1BirdyPoints")
    private String team1BirdyPoints;
    @Size(max = 100)
    @Column(name = "team2BirdyPoints")
    private String team2BirdyPoints;
    @Size(max = 100)
    @Column(name = "gameDescription")
    private String gameDescription;

    public GameLBLT() {
    }

    public GameLBLT(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public String getTeam1presses() {
        return team1presses;
    }

    public void setTeam1presses(String team1presses) {
        this.team1presses = team1presses;
    }

    public String getTeam2presses() {
        return team2presses;
    }

    public void setTeam2presses(String team2presses) {
        this.team2presses = team2presses;
    }

    public String getTeam1GreenyPoints() {
        return team1GreenyPoints;
    }

    public void setTeam1GreenyPoints(String team1GreenyPoints) {
        this.team1GreenyPoints = team1GreenyPoints;
    }

    public String getTeam2GreenyPoints() {
        return team2GreenyPoints;
    }

    public void setTeam2GreenyPoints(String team2GreenyPoints) {
        this.team2GreenyPoints = team2GreenyPoints;
    }

    public String getTeam1BirdyPoints() {
        return team1BirdyPoints;
    }

    public void setTeam1BirdyPoints(String team1BirdyPoints) {
        this.team1BirdyPoints = team1BirdyPoints;
    }

    public String getTeam2BirdyPoints() {
        return team2BirdyPoints;
    }

    public void setTeam2BirdyPoints(String team2BirdyPoints) {
        this.team2BirdyPoints = team2BirdyPoints;
    }

    public String getGameDescription() {
        return gameDescription;
    }

    public void setGameDescription(String gameDescription) {
        this.gameDescription = gameDescription;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gameId != null ? gameId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameLBLT)) {
            return false;
        }
        GameLBLT other = (GameLBLT) object;
        if ((this.gameId == null && other.gameId != null) || (this.gameId != null && !this.gameId.equals(other.gameId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.GameLBLT[ gameId=" + gameId + " ]";
    }
    
}
