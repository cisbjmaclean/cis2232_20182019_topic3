package info.hccis.admin.model.entity;

import java.util.ArrayList;

public class ScoreCard {

    private String userId = "";
    private String userName = "";
    private Integer gameId;
    private ArrayList<String> gameHoleScoreArrayList = new ArrayList();
    private ArrayList<String> gameHoleNumberArrayList = new ArrayList();
    private ArrayList<String> gameHoleParArrayList = new ArrayList();
    
    //temp to be replaced by database
    private static int[] pars = {4,4,5,3,4,5,3,4,4,5,4,3,4,4,3,4,4,5};

    public ScoreCard() {
        for (int i = 1; i < 19; i++) {
            gameHoleNumberArrayList.add("" + i);
            gameHoleParArrayList.add(""+pars[i-1]);
        }
    }
    public ScoreCard(Integer gameId) {
        this.gameId = gameId;
        for (int i = 1; i < 19; i++) {
            gameHoleScoreArrayList.add("");
            gameHoleNumberArrayList.add("" + i);
            gameHoleParArrayList.add(""+pars[i-1]);
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public ArrayList<String> getGameHoleScoreArrayList() {
        return gameHoleScoreArrayList;
    }

    public void setGameHoleScoreArrayList(ArrayList<String> gameHoleScoreArrayList) {
        this.gameHoleScoreArrayList = gameHoleScoreArrayList;
    }

    public ArrayList<String> getGameHoleNumberArrayList() {
        return gameHoleNumberArrayList;
    }

    public void setGameHoleNumberArrayList(ArrayList<String> gameHoleNumberArrayList) {
        this.gameHoleNumberArrayList = gameHoleNumberArrayList;
    }

    public ArrayList<String> getGameHoleParArrayList() {
        return gameHoleParArrayList;
    }

    public void setGameHoleParArrayList(ArrayList<String> gameHoleParArrayList) {
        this.gameHoleParArrayList = gameHoleParArrayList;
    }

    public static int[] getPars() {
        return pars;
    }

    public static void setPars(int[] pars) {
        ScoreCard.pars = pars;
    }

}
