package info.hccis.admin.util;

import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.Game;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.service.CodeService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

/**
 * This class will be used to hold generic code related methods.
 *
 * @author bjmaclean
 * @since 20170503
 */
public class UtilGames {

    /**
     * This method will load the course name in the collection of games passed
     * in.
     *
     * @since 20170513
     * @author BJM
     *
     */
    public static void setCourseNamesForGames(HttpServletRequest request, ArrayList<Game> games) {
        for (int i = 0; i < games.size(); i++) {
            Game current = games.get(i);
            current = getGameCourseName(current, request);
            System.out.println("Set current course to:" + current.getCourseName());
            games.set(i, current);
        }
    }

    /**
     * This method will get the course name for the course type.
     *
     * @since 20170513
     * @author BJM
     *
     * @return the game with the course name set.
     */
    public static Game getGameCourseName(Game game, HttpServletRequest request) {
        ArrayList<CodeValue> golfCourseTypes = (ArrayList<CodeValue>) request.getSession().getAttribute("golfCourseTypes");
        int type = game.getCourseType();
        System.out.println("Getting course name for courseType=" + type);
        for (CodeValue cv : golfCourseTypes) {

            if (cv.getCodeValueSequence() == type) {
                System.out.println("Found course name for courseType=" + type + " =" + cv.getEnglishDescription());
                game.setCourseName(cv.getEnglishDescription());
            }
        }
        return game;
    }

    /**
     * This method will take the scores and the pars. It will loop through and
     * add up the Chicago points based on the chicago points array in the
     * session.
     *
     * @since 20170513
     * @author BJM
     *
     * @param request
     * @param gameHoleParArrayList
     * @param playerScore
     * @return the points
     */
    public static int getChicagoPoints(HttpServletRequest request, ArrayList<String> gameHoleParArrayList, int[] playerScore) {
        int playerPoints = 0;
        for (int i = 0; i < playerScore.length; i++) {
            if (playerScore[i] > 0) {
                int difference = Integer.parseInt(gameHoleParArrayList.get(i)) - playerScore[i];
                difference += 1;
                int[] points = (int[]) request.getSession().getAttribute("chicagoPoints");
                if (difference >= 0 && difference < points.length) {
                    playerPoints += points[difference];
                }
            }
        }
        return playerPoints;
    }

    /**
     * Find birdies based on scores and par
     * @since 20170524
     * @author BJM
     */
    public static String[] findBirdies(int teamNumber, int[][] all18, ArrayList<String> gameHoleParArrayList) {
        String[] temp = new String[18];

//        for (int playerIndex = (teamNumber*2-2); playerIndex < teamNumber*2-1; playerIndex++) {
            for (int i = 0; i < 18; i++) {
                if((all18[teamNumber*2-2][i]>0&&all18[teamNumber*2-2][i]<Integer.parseInt(gameHoleParArrayList.get(i)))
                        ||(all18[teamNumber*2-1][i]>0&&all18[teamNumber*2-1][i]<Integer.parseInt(gameHoleParArrayList.get(i)))){
                    temp[i] = ""+(i+1);
                }else{
                    temp[i] = "";
                }
            }
//        }
        return temp;
    }

    /**
     * This method will take the give the chicagoPoints base one score.
     *
     * @since 20170513
     * @author BJM
     *
     * @return the points
     */
    public static int getChicagoPoints(HttpServletRequest request, int par, int score) {
        int playerPoints = 0;
        int difference = par - score;
        difference += 1;
        int[] points = (int[]) request.getSession().getAttribute("chicagoPoints");
        if (difference >= 0 && difference < points.length) {
            playerPoints += points[difference];
        }

        return playerPoints;
    }
}
