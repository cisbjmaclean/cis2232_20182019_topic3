package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.GamePlayerRepository;
import info.hccis.admin.data.springdatajpa.GameRepository;
import info.hccis.admin.data.springdatajpa.ScoreRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.jpa.Game;
import info.hccis.admin.model.entity.ScoreCard;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.model.jpa.GamePlayer;
import info.hccis.admin.model.jpa.Score;
import info.hccis.admin.util.UtilGamePlayer;
import info.hccis.admin.util.UtilGames;
import java.util.ArrayList;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GamePlayerController {

    private final ScoreRepository sr;
    private final GamePlayerRepository gpr;
    private final UserRepository ur;
    private final GameRepository gr;

    @Autowired
    public GamePlayerController(ScoreRepository sr, GamePlayerRepository gpr, UserRepository ur, GameRepository gr) {
        this.sr = sr;
        this.gpr = gpr;
        this.ur = ur;
        this.gr = gr;
    }
    
    @RequestMapping("/gameplayer/list")
    public String gameList(Model model, HttpServletRequest request) {
        System.out.println("In gameDelete gameId="+request.getParameter("id"));
        String id = request.getParameter("id");
        System.out.println("In gameList id ="+id);
        ArrayList<GamePlayer> gamePlayers = (ArrayList<GamePlayer>) gpr.findByGameId(Integer.parseInt(id));
        
        for(GamePlayer current: gamePlayers){
            User theGuy = ur.findOne(current.getUserId());
            current.setUser(theGuy);
        }
        model.addAttribute("gamePlayers", gamePlayers);
        Game game = gr.findOne(Integer.parseInt(id));
        game = UtilGames.getGameCourseName(game, request);
        request.getSession().setAttribute("game", game);
        return "/gameplayer/list";
    }

    @RequestMapping("/gameplayer/delete")
    public String gameDelete(Model model, HttpServletRequest request) {
        String id = request.getParameter("id");
        String userId = request.getParameter("userId");
        System.out.println("In gameDelete gameId="+id+" userId="+userId);
        ArrayList<GamePlayer> gamePlayers = (ArrayList<GamePlayer>) gpr.findByGameIdAndUserId(Integer.parseInt(id), Integer.parseInt(userId));
        for(GamePlayer current: gamePlayers){
            gpr.delete(current.getGamePlayerId());
        }
        ArrayList<GamePlayer> newGamePlayers = new ArrayList();
        newGamePlayers = (ArrayList<GamePlayer>) gpr.findAll();

        for(GamePlayer current: newGamePlayers){
            User theGuy = ur.findOne(current.getUserId());
            current.setUser(theGuy);
        }

        model.addAttribute("gamePlayers", newGamePlayers);

        return "/gameplayer/list";
    }
    
        @RequestMapping("/gameplayer/add")
    public String gamePlayerAdd(Model model, HttpServletRequest request) {

        String gameIdString = request.getParameter("id");
        int gameId = Integer.parseInt(gameIdString);
        ScoreCard scorecard = new ScoreCard(Integer.parseInt(gameIdString));
        System.out.println("In scoreAdd gameId=" + gameIdString);

        //If there is a userId then we know who we are adding for
        String userIdString = request.getParameter("userId");
        System.out.println("In scoreAdd userId=" + userIdString);
        int userId;
        
        //Remove users from drop down that already have scores.
        ArrayList<User> users = (ArrayList<User>) request.getSession().getAttribute("users");
        ArrayList<User> newUsers = new ArrayList();
        for(User current: users){
            ArrayList<GamePlayer> exists = (ArrayList<GamePlayer>) gpr.findByGameIdAndUserId(gameId, current.getUserId());
            if(exists.size() == 0){
                newUsers.add(current);
            }
        }
        model.addAttribute("newUsers", newUsers);
        
        if (userIdString != null && !userIdString.isEmpty()) {

            userId = Integer.parseInt(userIdString);
            scorecard.setUserId(userIdString);
            User user = ur.findOne(userId);
            System.out.println("found user for "+userId+" "+user.getFirstName()+" "+user.getLastName());
            scorecard.setUserName(user.getFirstName()+" "+user.getLastName());
            //try to get the existing scores
            ArrayList<Score> scores = (ArrayList<Score>) sr.findByGameIdAndUserId(gameId, userId);
            Collections.sort(scores);

            for (Score score : scores) {
                System.out.println("Setting scores for hole number index="+(score.getHoleNumber()-1));
                scorecard.getGameHoleScoreArrayList().set(score.getHoleNumber()-1 ,"" + score.getShotsTaken());
                scorecard.getGameHoleParArrayList().set(score.getHoleNumber()-1, "" + score.getPar());
                scorecard.getGameHoleNumberArrayList().set(score.getHoleNumber()-1,"" + score.getHoleNumber());
            }
        }

        model.addAttribute("score", scorecard);
        return "/gameplayer/add";
    }

    @RequestMapping("/gameplayer/addSubmit")
    public String gamePlayerAddSubmit(Model model, HttpServletRequest request, @ModelAttribute("score") ScoreCard score) {

        System.out.println("In scoreAdd gameId=" + score.getGameId() + " userId=" + score.getUserId());

        //delete their existing

        ArrayList<Score> existingScores = (ArrayList<Score>) sr.findByGameIdAndUserId(score.getGameId(), Integer.parseInt(score.getUserId()));
        for (Score existingScore : existingScores) {
            sr.delete(existingScore.getScoreId());
        }

        //When editing will want to get the score id added to the object.
        int[] points = (int[]) request.getSession().getAttribute("chicagoPoints");
        int playerPoints = 0;
        int playerScore = 0;

        //Get the game player based on the userId and gameId.
        GamePlayer gp = UtilGamePlayer.getGamePlayerForUserIdAndGameId(gpr, Integer.parseInt(score.getUserId()), score.getGameId());
        
        for (int i = 0; i < 18; i++) {
            System.out.println("score entered=" + score.getGameHoleScoreArrayList().get(i));
            System.out.println("hole number entered=" + score.getGameHoleNumberArrayList().get(i));
            String nextScore = score.getGameHoleScoreArrayList().get(i);
            if (nextScore != null && !nextScore.isEmpty() && !nextScore.equals("0")) {
                Score newScore = new Score(0, score.getGameId(), Integer.parseInt(score.getUserId()));
                newScore.setHoleNumber(Integer.parseInt(score.getGameHoleNumberArrayList().get(i)));
                newScore.setShotsTaken(Integer.parseInt(nextScore));
                newScore.setPar(Integer.parseInt(score.getGameHoleParArrayList().get(i)));
                //**************************************************************
                //index of points start at 0(bogey to 4(double eagle). If they had a bogey
                //the difference below would be -1.  If they have a double eagle it would be 
                //+3.  
                //**************************************************************
                newScore.setPoints(0);
                int difference = newScore.getPar() - newScore.getShotsTaken();
                difference += 1;
                if (difference >= 0 && difference < points.length) {
                    newScore.setPoints(points[difference]);
                    playerPoints += points[difference];
                }
                playerScore += newScore.getShotsTaken();
                sr.save(newScore);
            }
        }
        gp.setPoints(playerPoints);
        gp.setScore(playerScore);
        //***********************************
        //Persist the gamePlayer
        //***********************************
        gpr.save(gp);

        //**********************************************************************
        // Send the user to the particular game players page.  The new score should 
        // be now added to the list.
        //**********************************************************************
        ArrayList<GamePlayer> gamePlayers = (ArrayList<GamePlayer>) gpr.findByGameId(score.getGameId());

        for (int i = 0; i < gamePlayers.size(); i++) {
            GamePlayer current = new GamePlayer();
            current = gamePlayers.get(i);
            User theGuy = ur.findOne(current.getUserId()); //Adding the user to the gameplayer (name etc...)
            current.setUser(theGuy);
            gamePlayers.set(i, current);
        }

        model.addAttribute("gamePlayers", gamePlayers);
        Game game = gr.findOne(score.getGameId());
        game = UtilGames.getGameCourseName(game, request);
        request.getSession().setAttribute("game", game);
        return "/gameplayer/list";
    }

}
