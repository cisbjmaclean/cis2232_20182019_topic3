--
-- Database: yesis_golf
--
DROP DATABASE cis2232_golf;
create database cis2232_golf;
use cis2232_golf;

-- --------------------------------------------------------
grant select, insert, update, delete on cis2232_golf.*
             to 'cis2232_admin'@'localhost'
             identified by 'Test1234';
flush privileges;


--
-- Table structure for table CodeType
--

CREATE TABLE CodeType (
  codeTypeId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Table structure for table CodeValue
--

CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Table structure for table User
--

CREATE TABLE User (
  userId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  lastName varchar(100) NOT NULL,
  firstName varchar(100) NOT NULL,
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  additional1 varchar(100),
  additional2 varchar(100),
  createdDateTime varchar(20) DEFAULT NULL COMMENT 'When user was created.'
);

--
-- Indexes for dumped tables
--

--
-- Indexes for table CodeType
--

CREATE TABLE Game (
  gameId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  courseType int(3) COMMENT 'Unique user name for app',
  createdDate varchar(100) DEFAULT NULL COMMENT 'When user was created.'
); 

CREATE TABLE GamePlayer (
  gamePlayerId int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  gameId int(3) NOT NULL,
  userId int(3) NOT NULL,
  score int(3),
  points int(3)
); 


CREATE TABLE Score (
  scoreId int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  gameId int(3) NOT NULL,
  userId int(3) NOT NULL,
  holeNumber int(3),
  shotsTaken int(3),
  par int(3),
  points int(3)
); 

CREATE TABLE GolfGroup  (
  groupId int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  gameId int(3) NOT NULL,
  userId1 int(3),
  userId2 int(3),
  userId3 int(3),
  userId4 int(3)
); 

CREATE TABLE GameLBLT  (
  gameId int(3) NOT NULL PRIMARY KEY ,
  team1presses varchar(100),
  team2presses varchar(100),
  team1GreenyPoints varchar(100),
  team2GreenyPoints varchar(100),
  team1BirdyPoints varchar(100),
  team2BirdyPoints varchar(100),
  gameDescription varchar(100)
); 

INSERT INTO CodeType (codeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 'User Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(2, 'Golf Courses', NULL, '2017-05-03 09:15:41', NULL, '2017-05-03 09:15:41', 'ADMIN'),
(3, 'Chicago Points', NULL, '2017-05-03 09:15:41', NULL, '2017-05-03 09:15:41', 'ADMIN');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 1, 'Admin', 'Admin', 'AdminFR', 'AdminFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(1, 2, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(2, 3, 'Fox Meadow', 'FM', 'Fox MeadowFR', 'FMFR', '2017-05-03 09:15:58', 'admin', '2017-05-03 09:15:58', 'admin'),
(2, 4, 'Stanhope', 'Stanhope', 'StanhopeFR', 'StanhopeFR', '2017-05-03 09:16:12', 'admin', '2017-05-03 09:16:12', 'admin'),
(3, 5, '1', '1', '1', '1', '2017-05-03 09:16:12', 'admin', '2017-05-03 09:16:12', 'admin'),
(3, 6, '2', '2', '2', '2', '2017-05-03 09:16:12', 'admin', '2017-05-03 09:16:12', 'admin'),
(3, 7, '4','4', '4', '4', '2017-05-03 09:16:12', 'admin', '2017-05-03 09:16:12', 'admin'),
(3, 8, '8','8', '8', '8', '2017-05-03 09:16:12', 'admin', '2017-05-03 09:16:12', 'admin'),
(3, 9, '16','8', '16', '16', '2017-05-03 09:16:12', 'admin', '2017-05-03 09:16:12', 'admin');

INSERT INTO `User` (`userId`, `username`, `password`, `lastName`, `firstName`, `userTypeCode`, `additional1`, `additional2`, `createdDateTime`) VALUES
(1, 'bmaclean', '202cb962ac59075b964b07152d234b70', 'MacLean', 'BJ', 1, NULL, NULL, '2017-05-10 13:43:38'),
(2, 'wmcnally', '202cb962ac59075b964b07152d234b70', 'McNally', 'Wayd', 2, NULL, NULL, '2017-05-10 13:43:55'),
(3, 'cmacewen', '202cb962ac59075b964b07152d234b70', 'MacEwen', 'Craig', 2, NULL, NULL, '2017-05-10 13:44:17'),
(4, 'dsmith', '202cb962ac59075b964b07152d234b70', 'Smith', 'David', 2, NULL, NULL, '2017-05-10 13:44:31'),
(5, 'datkins', '202cb962ac59075b964b07152d234b70', 'Atkins', 'Ham', 2, NULL, NULL, '2017-05-10 13:44:47'),
(6, 'jkirkpatrick', '202cb962ac59075b964b07152d234b70', 'Kirkpatrick', 'Jeff', 2, NULL, NULL, '2017-05-10 13:44:31'),
(7, 'sleclair', '202cb962ac59075b964b07152d234b70', 'Leclair', 'Skipper', 2, NULL, NULL, '2017-05-10 13:44:31'),
(8, 'cdowney', '202cb962ac59075b964b07152d234b70', 'Downey', 'Cory', 2, NULL, NULL, '2017-05-10 13:44:31');
