package info.hccis.hello.entity;

/**
 * 
 * @author bjmaclean
 * @since Oct 6, 2017
 */
public class Person {
    private String name;
    private String address;
    private String email;
    private String courseSelected;

    public String getCourseSelected() {
        return courseSelected;
    }

    public void setCourseSelected(String courseSelected) {
        this.courseSelected = courseSelected;
    }

    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", address=" + address + ", email=" + email + ", courseSelected=" + courseSelected + '}';
    }

    
}
