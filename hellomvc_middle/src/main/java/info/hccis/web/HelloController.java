package info.hccis.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/")
    public String showHome(Model model) {
        
        //This will send the user to the welcome.html page.
        return "hello/welcome";
    }

    @RequestMapping("/sayhello")
    public String sayHelloExample(Model model) {
        
        System.out.println("BJM - Hello");
        
        int test = (int) (Math.random()*10);
        return "hello/newOne";
        
    }


    @RequestMapping("/goToNew2")
    public String goToNewTwo(Model model) {
        System.out.println("in controller for /newOne");
        return "hello/newTwo";

        /*
        After this have to make sure that this html page exists. This would be 
        in the WEB-INF/thymeleaf/camper/
         */
    }

}
