package info.hccis.camper.web;

import info.hccis.camper.dao.CamperDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    @RequestMapping("/")
    public String showHome(Model model) {

        //Get the campers from the database
        model.addAttribute("campers", CamperDAO.selectAll());
        
        //This will send the user to the list page
        return "camper/list";
    }
    @RequestMapping("/other/help")
    public String showHelp() {

        //This will send the user to the help view.
        return "other/help";
    }
    @RequestMapping("/other/about")
    public String showAbout() {

        //This will send the user to the help view.
        return "other/about";
    }

}
