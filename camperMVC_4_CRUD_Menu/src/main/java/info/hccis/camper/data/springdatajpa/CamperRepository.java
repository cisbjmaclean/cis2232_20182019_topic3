package info.hccis.camper.data.springdatajpa;

import info.hccis.camper.entity.Camper;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CamperRepository extends CrudRepository<Camper, Integer> {

}