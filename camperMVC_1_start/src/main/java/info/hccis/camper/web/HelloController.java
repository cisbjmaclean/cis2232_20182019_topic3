package info.hccis.camper.web;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.entity.Camper;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/")
    public String showHome(Model model) {

        //Get the campers from the database
        ArrayList<Camper> campers = CamperDAO.selectAll();
        System.out.println("BJM-found "+campers.size()+" campers.  Going to welcome page");
        model.addAttribute("campers", campers);
        
        //This will send the user to the welcome.html page.
        return "camper/welcome";
    }

    @RequestMapping("/sayhello" )
    public String sayHello(Model model) {        
        
        //This will send the user to the welcome.html page.
        return "hello/newOne";
    }

    
    @RequestMapping("/newOne")
    public String showNewOne(Model model) {
        System.out.println("in controller for /newOne");
        return "hello/newOne";

        /*
        After this have to make sure that this html page exists. This would be 
        in the WEB-INF/thymeleaf/camper/
         */
    }

}
